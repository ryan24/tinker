package com.sg.labs.tinker;

import com.sg.labs.tinker.resources.Hello;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig extends ResourceConfig {
    public AppConfig() {
        register(Hello.class);
    }
}
